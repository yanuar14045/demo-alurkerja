package com.alurkerja.leave.review;

import com.alurkerja.core.service.ActivityLogService;
import com.alurkerja.core.service.BpmnService;
import com.alurkerja.core.service.CamundaProcessService;
import com.alurkerja.leave.Leave;
import com.alurkerja.leave.LeaveRepository;
import org.springframework.stereotype.Service;


@Service
public class ReviewService extends BpmnService<Leave, ReviewDto, ReviewVariable, LeaveRepository> {
    protected ReviewService(LeaveRepository leaveRepository, CamundaProcessService camundaProcessService, ActivityLogService activityLogService) {
        super(leaveRepository, camundaProcessService, activityLogService);
    }

    @Override
    protected void sendStartNotification(Leave entity, ReviewVariable variable, ReviewDto dto) {

    }

    @Override
    protected void sendSubmitNotification(String taskDefinitionKey, String decision, Leave entity, ReviewVariable variable, ReviewDto dto) {

    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }
}
