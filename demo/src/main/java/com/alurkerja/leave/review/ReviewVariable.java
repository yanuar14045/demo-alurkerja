package com.alurkerja.leave.review;

import com.alurkerja.core.entity.BaseVariable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewVariable extends BaseVariable {
    private String reason;
}
