package com.alurkerja.leave.review;

import com.alurkerja.core.controller.UserTaskController;
import com.alurkerja.leave.Leave;
import com.alurkerja.leave.LeaveRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bpmn/leave/review")
public class ReviewController extends UserTaskController<Leave, ReviewDto, ReviewVariable, ReviewService, LeaveRepository> {
    public ReviewController(ReviewService reviewService) {
        super(reviewService, "review");
    }
}
