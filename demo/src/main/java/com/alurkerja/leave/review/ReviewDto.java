package com.alurkerja.leave.review;

import com.alurkerja.core.entity.BaseDto;
import com.alurkerja.leave.Leave;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewDto extends BaseDto<Leave, ReviewDto> {

}
