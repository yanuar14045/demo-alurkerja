package com.alurkerja.leave;

import com.alurkerja.core.entity.BaseDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LeaveDto extends BaseDto<Leave, LeaveDto> {
    private String name;
    private String reason;
}
