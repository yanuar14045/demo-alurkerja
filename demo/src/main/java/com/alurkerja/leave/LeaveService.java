package com.alurkerja.leave;

import com.alurkerja.core.service.ActivityLogService;
import com.alurkerja.core.service.BpmnService;
import com.alurkerja.core.service.CamundaProcessService;
import com.alurkerja.core.service.email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeaveService extends BpmnService<Leave, LeaveDto, LeaveVariable, LeaveRepository> {

    @Autowired
    EmailService emailService;

    protected LeaveService(LeaveRepository leaveRepository, CamundaProcessService camundaProcessService, ActivityLogService activityLogService) {
        super(leaveRepository, camundaProcessService, activityLogService);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }

    @Override
    protected void sendStartNotification(Leave entity, LeaveVariable variable, LeaveDto dto) {
        /*
        Fill notification method here
         */
    }

    @Override
    protected void sendSubmitNotification(String taskDefinitionKey, String decision, Leave entity, LeaveVariable variable, LeaveDto dto) {
        /*
        Fill notification method here
         */
    }
}
