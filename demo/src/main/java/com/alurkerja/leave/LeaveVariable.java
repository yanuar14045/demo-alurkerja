package com.alurkerja.leave;

import com.alurkerja.core.entity.BaseVariable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LeaveVariable extends BaseVariable {
    String a;
    String b;
}
