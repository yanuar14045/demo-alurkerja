package com.alurkerja.leave;

import com.alurkerja.core.controller.BpmnController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping("/bpmn/leave")
public class LeaveController extends BpmnController<Leave, LeaveDto, LeaveVariable, LeaveService, LeaveRepository> {
    public LeaveController(LeaveService leaveService) {
        super(leaveService);
    }
}
