package com.alurkerja.leave;

import com.alurkerja.core.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeaveRepository extends CrudRepository<Leave> {
}
