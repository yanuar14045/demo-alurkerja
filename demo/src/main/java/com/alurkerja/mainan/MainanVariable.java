package com.alurkerja.mainan;

import com.alurkerja.core.entity.BaseVariable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MainanVariable extends BaseVariable {
    String a;
    String b;
}
