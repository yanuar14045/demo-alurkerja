package com.alurkerja.mainan;

import com.alurkerja.core.controller.BpmnController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping("/bpmn/mainan")
public class MainanController extends BpmnController<Mainan, MainanDto, MainanVariable, MainanService, MainanRepository> {
    public MainanController(MainanService mainanService) {
        super(mainanService);
    }
}
