package com.alurkerja.mainan;

import com.alurkerja.core.exception.AlurKerjaException;
import com.alurkerja.core.service.ActivityLogService;
import com.alurkerja.core.service.BpmnService;
import com.alurkerja.core.service.CamundaProcessService;
import com.alurkerja.core.service.email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MainanService extends BpmnService<Mainan, MainanDto, MainanVariable, MainanRepository> {

    @Autowired
    EmailService emailService;

    protected MainanService(MainanRepository leaveRepository, CamundaProcessService camundaProcessService, ActivityLogService activityLogService) {
        super(leaveRepository, camundaProcessService, activityLogService);
    }

    @Override
    public Map<String, Object> start(MainanVariable variable, MainanDto dto) throws AlurKerjaException {
        return super.start(variable, dto);
    }

    @Override
    protected void sendStartNotification(Mainan entity, MainanVariable variable, MainanDto dto) {

    }

    @Override
    protected void sendSubmitNotification(String taskDefinitionKey, String decision, Mainan entity, MainanVariable variable, MainanDto dto) {

    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }
}
