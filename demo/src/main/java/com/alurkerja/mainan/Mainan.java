package com.alurkerja.mainan;

import com.alurkerja.core.entity.BpmnEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class Mainan extends BpmnEntity {
    private String name;
    private String reason;
    private Date when;

    @Column
    @Convert(converter = StringListConverter.class)
    private List<String> tags;
}
