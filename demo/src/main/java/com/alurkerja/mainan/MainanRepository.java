package com.alurkerja.mainan;

import com.alurkerja.core.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MainanRepository extends CrudRepository<Mainan> {
}
