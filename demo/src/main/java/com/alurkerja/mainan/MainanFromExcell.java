package com.alurkerja.mainan;

import com.alurkerja.core.util.ExcelReader;

public class MainanFromExcell extends ExcelReader<Mainan> {
    @Override
    public int skippedRow() {
        return 2;
    }

    @Override
    public Mainan perLine(Object[] columns) {
        Mainan mainan = new Mainan();
        mainan.setName((String) columns[0]);
        mainan.setReason(String.valueOf(columns[1]));
        return mainan;
    }
}
