package com.alurkerja.mainan;

import com.alurkerja.core.anotation.FormReference;
import com.alurkerja.core.entity.BaseDto;
import com.alurkerja.crud.category.Category;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Lob;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class MainanDto extends BaseDto<Mainan, MainanDto> {
    private String name;

    @Lob
    private String reason;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date when;

    @FormReference(label = "Category", type = "select", url = "/crud/category", key = "id", value = "name")
    Category category;

    @FormReference(label = "Tags", type = "checkbox", jsonValues = "[{\"id\":\"sample1\", \"name\":\"sample1\"}, {\"id\":\"sample2\",\"name\":\"sample2\"}]", key = "id", value = "name")
    private List<String> tags;

    @FormReference(label = "Gender", required = true, type = "radio", jsonValues = "[{\"id\":\"male\", \"name\":\"MALE\"}, {\"id\":\"female\", \"name\":\"FEMALE\"}]", key = "id", value = "name")
    private String gender;
}
