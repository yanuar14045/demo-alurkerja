package com.alurkerja.crud.article;

import com.alurkerja.core.entity.BaseDto;
import com.alurkerja.crud.category.Category;
import com.alurkerja.crud.category.CategoryDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToOne;

@Getter
@Setter
public class ArticleDto extends BaseDto<Article, ArticleDto> {
    private String title;
    private String content;
    @ManyToOne
    private CategoryDto category;
}
