package com.alurkerja.crud.article;

import com.alurkerja.core.controller.CrudController;
import com.alurkerja.core.repository.search.BaseSearchSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/crud/article")
public class ArticleController extends CrudController<Article, ArticleDto, ArticleService, ArticleRepository> {

    protected ArticleController(ArticleService articleService) {
        super(articleService);
    }

}
