package com.alurkerja.crud.article;

import com.alurkerja.core.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends CrudRepository<Article> {
}
