package com.alurkerja.crud.article;

import com.alurkerja.core.exception.AlurKerjaException;
import com.alurkerja.core.service.CrudService;
import com.alurkerja.crud.category.Category;
import com.alurkerja.crud.category.CategoryRepository;
import org.camunda.bpm.engine.BadUserRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArticleService extends CrudService<Article, ArticleDto, ArticleRepository> {
    @Autowired
    CategoryRepository categoryRepository;

    protected ArticleService(ArticleRepository articleRepository) {
        super(articleRepository);
    }

    @Override
    public Article createFromDto(ArticleDto dto) throws AlurKerjaException {
        Optional<Category> categoryOptional = categoryRepository.findById(dto.getCategory().getId());
        if(!categoryOptional.isPresent()){
            throw new AlurKerjaException("Invalid category");
        }

        Article article = dto.fromDto();
        article.setCategory(categoryOptional.get());
        return this.create(article);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }
}
