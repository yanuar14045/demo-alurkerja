package com.alurkerja.crud.article;

import com.alurkerja.core.entity.CrudEntity;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.alurkerja.crud.category.Category;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Article extends CrudEntity {
    private String title;
    private String content;
    @ManyToOne
    private Category category;
}
