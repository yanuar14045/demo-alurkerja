package com.alurkerja.crud.article;

import com.alurkerja.core.repository.search.BaseSearchSpecification;

public class ArticleSpecification extends BaseSearchSpecification<Article, ArticleDto> {

    public ArticleSpecification(ArticleDto dto) {
        super(dto);
    }
}
