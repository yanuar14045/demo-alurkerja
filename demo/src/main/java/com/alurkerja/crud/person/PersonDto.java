package com.alurkerja.crud.person;

import com.alurkerja.core.entity.BaseDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDto extends BaseDto<Person, PersonDto> {
    private String name;
    private Integer age;
}