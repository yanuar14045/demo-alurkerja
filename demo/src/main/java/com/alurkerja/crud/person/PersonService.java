package com.alurkerja.crud.person;

import com.alurkerja.core.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class PersonService extends CrudService<Person, PersonDto, PersonRepository> {
    Logger logger = Logger.getLogger(PersonService.class.getName());

    @Autowired
    public PersonService(PersonRepository personRepository) {
        super(personRepository);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }
}
