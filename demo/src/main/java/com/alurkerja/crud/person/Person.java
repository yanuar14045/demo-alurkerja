package com.alurkerja.crud.person;

import com.alurkerja.core.entity.CrudEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Person extends CrudEntity {
    private String name;
    private Integer age;
}
