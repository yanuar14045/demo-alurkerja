package com.alurkerja.crud.person;

import com.alurkerja.core.controller.CrudController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping("/crud/person")
public class PersonController extends CrudController<Person, PersonDto, PersonService, PersonRepository> {
    Logger logger = Logger.getLogger(getClass().getName());
    protected PersonController(PersonService service) {
        super(service);
    }
}
