package com.alurkerja.crud.category;

import com.alurkerja.core.repository.CrudRepository;
import com.alurkerja.core.service.CrudService;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends CrudService<Category, CategoryDto, CategoryRepository> {
    protected CategoryService(CategoryRepository simpleJpaRepository) {
        super(simpleJpaRepository);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }

}
