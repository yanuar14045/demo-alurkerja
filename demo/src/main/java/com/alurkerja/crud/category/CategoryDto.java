package com.alurkerja.crud.category;

import com.alurkerja.core.entity.BaseDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Lob;
import java.util.UUID;

@Getter
@Setter
public class CategoryDto extends BaseDto<Category, CategoryDto> {
    private String name;
    private String tag;

    @Lob
    private String description;
}
