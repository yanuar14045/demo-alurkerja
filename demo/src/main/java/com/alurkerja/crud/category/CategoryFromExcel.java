package com.alurkerja.crud.category;

import com.alurkerja.core.util.ExcelReader;

public class CategoryFromExcel extends ExcelReader<Category> {
    @Override
    public int skippedRow() {
        return 0;
    }

    @Override
    public Category perLine(Object[] columns) {
        Category category = new Category();
        category.setName((String) columns[0]);
        category.setTag((String) columns[1]);
        return category;
    }
}
