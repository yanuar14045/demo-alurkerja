package com.alurkerja.crud.school;

import com.alurkerja.core.controller.CrudController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/crud/school")
public class SchoolController extends CrudController<School, SchoolDto, SchoolService, SchoolRepository> {

    protected SchoolController(SchoolService schoolService) {
        super(schoolService);
    }
}
