package com.alurkerja.crud.school;

import com.alurkerja.core.entity.BaseDto;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class SchoolDto extends BaseDto<School, SchoolDto> {
    private UUID id;
    private String name;
}
