package com.alurkerja.crud.school;

import com.alurkerja.core.entity.CrudEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class School extends CrudEntity {
    private String name;
    private String tag;
//    @OneToMany(mappedBy = "category")
//    private List<Article> articleList;
}
