package com.alurkerja.crud.school;

import com.alurkerja.core.repository.CrudRepository;
import com.alurkerja.core.service.CrudService;
import org.springframework.stereotype.Service;

@Service
public class SchoolService extends CrudService<School, SchoolDto, SchoolRepository> {
    protected SchoolService(SchoolRepository schoolRepository) {
        super(schoolRepository);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }

}
