package com.alurkerja.crud.jatahcuti;

import com.alurkerja.core.service.CrudService;
import com.alurkerja.crud.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JatahCutiService extends CrudService<JatahCuti, JatahCutiDto, JatahCutiRepository> {

    @Autowired
    CategoryRepository categoryRepository;

    protected JatahCutiService(JatahCutiRepository JatahCutiRepository) {
        super(JatahCutiRepository);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }
}
