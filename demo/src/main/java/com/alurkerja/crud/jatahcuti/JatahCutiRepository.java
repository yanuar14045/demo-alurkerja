package com.alurkerja.crud.jatahcuti;

import com.alurkerja.core.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JatahCutiRepository extends CrudRepository<JatahCuti> {
}
