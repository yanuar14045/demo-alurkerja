package com.alurkerja.crud.jatahcuti;

import com.alurkerja.core.controller.CrudController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/crud/jatahcuti")
public class JatahCutiController extends CrudController<JatahCuti, JatahCutiDto, JatahCutiService, JatahCutiRepository> {
    protected JatahCutiController(JatahCutiService JatahCutiService) {
        super(JatahCutiService);
    }
}
