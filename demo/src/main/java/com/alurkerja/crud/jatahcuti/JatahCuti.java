package com.alurkerja.crud.jatahcuti;

import com.alurkerja.core.entity.CrudEntity;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class JatahCuti extends CrudEntity {
    private String username;
    private String type;
    private Integer numOfDay;
}
