package com.alurkerja.crud.jatahcuti;

import com.alurkerja.core.entity.BaseDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JatahCutiDto extends BaseDto<JatahCuti,JatahCutiDto> {
    private String username;
    private String type;
    private Integer numOfDay;
}
