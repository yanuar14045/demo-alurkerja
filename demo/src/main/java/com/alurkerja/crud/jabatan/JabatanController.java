package com.alurkerja.crud.jabatan;

import com.alurkerja.core.controller.CrudController;
import com.alurkerja.core.entity.BpmnEntity;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

@RestController
@RequestMapping("/crud/jabatan")
public class JabatanController extends CrudController<Jabatan, JabatanDto, JabatanService, JabatanRepository> {

    protected JabatanController(JabatanService jabatanService) {
        super(jabatanService);
    }

    @PostMapping("/excel")
    public ResponseEntity<Object> createFromFile(MultipartFile file) throws IOException, InvalidFormatException {
        JabatanFromExcel categoryFromExcel = new JabatanFromExcel();
        List<Jabatan> categories = categoryFromExcel.read(file.getInputStream());
        for(Jabatan category : categories){
            this.crudService.create(category);
        }
        return success(file.getOriginalFilename() + " was uploaded");
    }

    @GetMapping("/excel")
    public void exportToExcell(HttpServletResponse httpServletResponse) throws IOException {
        OutputStream outputStream = httpServletResponse.getOutputStream();

        List<Jabatan> categories = this.crudService.findAll();
        JabatanToExcel categoryToExcel = new JabatanToExcel(categories);
        categoryToExcel.export(outputStream);
        httpServletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        httpServletResponse.setHeader("Content-Disposition", "attachment;filename=\"category.xlsx\"");

        outputStream.flush();
        httpServletResponse.flushBuffer();
    }

    @PostMapping(path = "/upload")
    public ResponseEntity<Object> uploadFile(@RequestParam MultipartFile file) throws IOException {

        return this.success(file.getOriginalFilename());
    }
}
