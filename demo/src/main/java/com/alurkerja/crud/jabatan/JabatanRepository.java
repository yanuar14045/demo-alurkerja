package com.alurkerja.crud.jabatan;

import com.alurkerja.core.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JabatanRepository extends CrudRepository<Jabatan> {
}
