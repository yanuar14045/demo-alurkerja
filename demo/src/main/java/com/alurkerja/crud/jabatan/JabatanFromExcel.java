package com.alurkerja.crud.jabatan;

import com.alurkerja.core.util.ExcelReader;

public class JabatanFromExcel extends ExcelReader<Jabatan> {
    @Override
    public int skippedRow() {
        return 0;
    }

    @Override
    public Jabatan perLine(Object[] columns) {
        Jabatan category = new Jabatan();
        category.setName((String) columns[0]);
        category.setTag((String) columns[1]);
        return category;
    }
}
