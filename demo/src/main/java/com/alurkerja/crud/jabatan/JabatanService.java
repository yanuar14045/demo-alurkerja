package com.alurkerja.crud.jabatan;

import com.alurkerja.core.repository.CrudRepository;
import com.alurkerja.core.service.CrudService;
import org.springframework.stereotype.Service;

@Service
public class JabatanService extends CrudService<Jabatan, JabatanDto, JabatanRepository> {
    protected JabatanService(JabatanRepository jabatanRepository) {
        super(jabatanRepository);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }

}
