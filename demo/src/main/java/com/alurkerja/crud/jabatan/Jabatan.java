package com.alurkerja.crud.jabatan;

import com.alurkerja.core.entity.CrudEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.validation.constraints.Max;

@Entity
@Getter
@Setter
public class Jabatan extends CrudEntity {
    private String name;
    private String tag;

    @Lob
    private String description;
//    @OneToMany(mappedBy = "category")
//    private List<Article> articleList;
}
