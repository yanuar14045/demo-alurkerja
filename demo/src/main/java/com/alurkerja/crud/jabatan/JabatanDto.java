package com.alurkerja.crud.jabatan;

import com.alurkerja.core.entity.BaseDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Lob;
import java.util.UUID;

@Getter
@Setter
public class JabatanDto extends BaseDto<Jabatan, JabatanDto> {
    private String name;
    private String tag;

    @Lob
    private String description;
}
