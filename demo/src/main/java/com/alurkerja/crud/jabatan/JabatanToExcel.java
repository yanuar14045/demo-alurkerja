package com.alurkerja.crud.jabatan;

import com.alurkerja.core.util.ExcelWriter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.ArrayList;
import java.util.List;

public class JabatanToExcel extends ExcelWriter<Jabatan> {
    public JabatanToExcel(List<Jabatan> categories) {
        super(categories);
    }

    @Override
    public List<String> getHeaders() {
        ArrayList<String> headers = new ArrayList<>();
        headers.add("Name");
        headers.add("Tag");
        return headers;
    }

    @Override
    public void getRows(Row row, Jabatan entity) {
        Cell cell = row.createCell(0);
        cell.setCellValue(entity.getName());

        Cell cell1 = row.createCell(1);
        cell1.setCellValue(entity.getTag());

        Cell cell2 = row.createCell(2);
        cell2.setCellValue(entity.getDescription());
    }
}
