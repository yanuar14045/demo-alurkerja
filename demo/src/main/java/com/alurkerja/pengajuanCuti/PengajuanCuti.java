package com.alurkerja.pengajuanCuti;

import com.alurkerja.core.entity.BpmnEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class PengajuanCuti extends BpmnEntity {
    private String name;
    private String reason;
}
