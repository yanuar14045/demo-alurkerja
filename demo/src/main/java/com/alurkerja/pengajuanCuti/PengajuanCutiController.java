package com.alurkerja.pengajuanCuti;

import com.alurkerja.core.controller.BpmnController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping("/bpmn/pengajuanCuti")
public class PengajuanCutiController extends BpmnController<PengajuanCuti, PengajuanCutiDto, PengajuanCutiVariable, PengajuanCutiService, PengajuanCutiRepository> {
    public PengajuanCutiController(PengajuanCutiService leaveService) {
        super(leaveService);
    }
}
