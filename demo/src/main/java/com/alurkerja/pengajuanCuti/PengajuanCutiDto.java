package com.alurkerja.pengajuanCuti;

import com.alurkerja.core.entity.BaseDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PengajuanCutiDto extends BaseDto<PengajuanCuti, PengajuanCutiDto> {
    private String name;
    private String reason;
}
