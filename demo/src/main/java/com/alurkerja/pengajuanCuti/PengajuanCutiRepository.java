package com.alurkerja.pengajuanCuti;

import com.alurkerja.core.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PengajuanCutiRepository extends CrudRepository<PengajuanCuti> {
}
