package com.alurkerja.pengajuanCuti;

import com.alurkerja.core.entity.BaseVariable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PengajuanCutiVariable extends BaseVariable {
    String a;
    String b;
}
