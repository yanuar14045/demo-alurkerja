package com.alurkerja.pengajuanCuti;

import com.alurkerja.core.service.ActivityLogService;
import com.alurkerja.core.service.BpmnService;
import com.alurkerja.core.service.CamundaProcessService;
import com.alurkerja.core.service.email.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PengajuanCutiService extends BpmnService<PengajuanCuti, PengajuanCutiDto, PengajuanCutiVariable, PengajuanCutiRepository> {

    @Autowired
    EmailService emailService;

    protected PengajuanCutiService(PengajuanCutiRepository leaveRepository, CamundaProcessService camundaProcessService, ActivityLogService activityLogService) {
        super(leaveRepository, camundaProcessService, activityLogService);
    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }

    @Override
    protected void sendStartNotification(PengajuanCuti entity, PengajuanCutiVariable variable, PengajuanCutiDto dto) {
        /*
        Fill notification method here
         */
    }

    @Override
    protected void sendSubmitNotification(String taskDefinitionKey, String decision, PengajuanCuti entity, PengajuanCutiVariable variable, PengajuanCutiDto dto) {
        /*
        Fill notification method here
         */
    }
}
