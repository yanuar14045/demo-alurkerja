package com.alurkerja.pengajuanCuti.ApprovalCircleLead;

import com.alurkerja.core.controller.UserTaskController;
import com.alurkerja.pengajuanCuti.PengajuanCuti;
import com.alurkerja.pengajuanCuti.PengajuanCutiRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bpmn/pengajuanCuti/ApprovalCircleLead")
public class PengajuanCutiApprovalCircleLeadController extends UserTaskController<PengajuanCuti, PengajuanCutiApprovalCircleLeadDto, PengajuanCutiApprovalCircleLeadVariable, PengajuanCutiApprovalCircleLeadService, PengajuanCutiRepository> {
    public PengajuanCutiApprovalCircleLeadController(PengajuanCutiApprovalCircleLeadService ApprovalCircleLeadService) {
        super(ApprovalCircleLeadService, "ApprovalCircleLead");
    }
}
