package com.alurkerja.pengajuanCuti.ApprovalCircleLead;

import com.alurkerja.core.service.ActivityLogService;
import com.alurkerja.core.service.BpmnService;
import com.alurkerja.core.service.CamundaProcessService;
import com.alurkerja.pengajuanCuti.PengajuanCuti;
import com.alurkerja.pengajuanCuti.PengajuanCutiRepository;
import org.springframework.stereotype.Service;


@Service
public class PengajuanCutiApprovalCircleLeadService extends BpmnService<PengajuanCuti, PengajuanCutiApprovalCircleLeadDto, PengajuanCutiApprovalCircleLeadVariable, PengajuanCutiRepository> {
    protected PengajuanCutiApprovalCircleLeadService(PengajuanCutiRepository pengajuanCutiRepository, CamundaProcessService camundaProcessService, ActivityLogService activityLogService) {
        super(pengajuanCutiRepository, camundaProcessService, activityLogService);
    }

    @Override
    protected void sendStartNotification(PengajuanCuti entity, PengajuanCutiApprovalCircleLeadVariable variable, PengajuanCutiApprovalCircleLeadDto dto) {

    }

    @Override
    protected void sendSubmitNotification(String taskDefinitionKey, String decision, PengajuanCuti entity, PengajuanCutiApprovalCircleLeadVariable variable, PengajuanCutiApprovalCircleLeadDto dto) {

    }

    @Override
    public String getCurrentUser() {
        return null;
    }

    @Override
    public String getCurrentGroup() {
        return null;
    }

    @Override
    public String getCurrentOrganization() {
        return null;
    }
}
