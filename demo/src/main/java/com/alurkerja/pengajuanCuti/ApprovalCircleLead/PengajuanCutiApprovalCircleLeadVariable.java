package com.alurkerja.pengajuanCuti.ApprovalCircleLead;

import com.alurkerja.core.entity.BaseVariable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PengajuanCutiApprovalCircleLeadVariable extends BaseVariable {
    private String reason;
}
