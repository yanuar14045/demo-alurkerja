package com.alurkerja.pengajuanCuti.ApprovalCircleLead;

import com.alurkerja.core.entity.BaseDto;
import com.alurkerja.pengajuanCuti.PengajuanCuti;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PengajuanCutiApprovalCircleLeadDto extends BaseDto<PengajuanCuti, PengajuanCutiApprovalCircleLeadDto> {

}
