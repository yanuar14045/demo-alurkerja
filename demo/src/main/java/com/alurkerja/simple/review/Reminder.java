package com.alurkerja.simple.review;

import com.alurkerja.core.service.email.EmailService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Reminder implements JavaDelegate {

    @Autowired
    EmailService emailService;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("CHECKING THIS : " + this.emailService);
        this.emailService.send("wisnu@javan.co.id", "udin@javan.co.id", "Hello world", "This is hello world");
    }
}
