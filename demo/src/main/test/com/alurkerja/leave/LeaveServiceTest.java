package com.alurkerja.leave;

import com.alurkerja.DemoApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = DemoApplication.class)
class LeaveServiceTest {

    @Autowired
    LeaveService leaveService;

    @Test
    public void getStatistic(){
        Map<String, Object> statistic = leaveService.getStatistic(null);

        Assertions.assertNotNull(statistic);
        System.out.println(statistic);
    }
}