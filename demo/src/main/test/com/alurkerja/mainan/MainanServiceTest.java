package com.alurkerja.mainan;

import com.alurkerja.DemoApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest(classes = DemoApplication.class)
class MainanServiceTest {

    @Autowired
    MainanService mainanService;

    @Test
    public void getProcessDefitionKey(){
        Assertions.assertEquals("Mainan", mainanService.getProcessDefinitionKey());
    }

    @Test
    public void availableDecisionTest(){
        HashMap<String, Object> decisions = mainanService.availableDecision("review");
        System.out.println(decisions);
        Assertions.assertTrue(decisions.size()>0);

    }
}