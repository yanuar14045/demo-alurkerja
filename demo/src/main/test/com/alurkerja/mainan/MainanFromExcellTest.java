package com.alurkerja.mainan;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MainanFromExcellTest {

    @Test
    public void read() throws IOException, InvalidFormatException {
        MainanFromExcell mainanFromExcell = new MainanFromExcell();
        File file = new File("/home/wisnu/Documents/sampleMainan.xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        List<Mainan> mainanList= mainanFromExcell.read(fileInputStream);
        System.out.println(mainanList.size());
        for(Mainan mainan : mainanList){
            System.out.println(mainan.getName() + " : " + mainan.getReason());
        }
    }
}