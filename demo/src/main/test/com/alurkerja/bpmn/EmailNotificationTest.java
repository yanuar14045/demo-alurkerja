package com.alurkerja.bpmn;

import com.alurkerja.DemoApplication;
import com.alurkerja.core.service.email.EmailService;
import com.alurkerja.core.service.email.GenericEmailNotification;
import com.alurkerja.leave.Leave;
import com.alurkerja.leave.LeaveDto;
import com.alurkerja.leave.LeaveVariable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@SpringBootTest(classes = DemoApplication.class)
public class EmailNotificationTest {

    @Autowired
    EmailService emailService;
    @Test
    public void sendSimpleMessageSingleRecipient(){
        this.emailService.sendSimpleMessage("notification@alurkerja.com", "wisnu@javan.co.id ", "Hello", "Hellow");
    }

    @Test
    public void sendSimpleMessageMultipleRecipient(){
        this.emailService.sendSimpleMessage("notification@alurkerja.com", new String[]{"wisnu@javan.co.id", "novi@javan.co.id"}, "Hello", "Hellow");
    }

    @Test
    public void templateEngine(){
        TemplateEngine templateEngine = new TemplateEngine();
        Context context = new Context();
        context.setVariable("name", "wisnu");
        context.setVariable("reason", "okok");
        String result = templateEngine.process("email/hello.html", context);

        System.out.println(result);
    }

    @Test
    public void sendGenericEmailNotification(){
//        Leave leave = new Leave();
//        LeaveVariable leaveVariable = new LeaveVariable();
//        LeaveDto leaveDto = new LeaveDto();
//        GenericEmailNotification<Leave, LeaveVariable, LeaveDto> genericEmailNotification = new GenericEmailNotification<>("email/hello.html", leave, leaveVariable, leaveDto);
//        String[] to = new String[1];
//        to[0] = "wisnu@javan.co.id";
//        genericEmailNotification.sendNotification(to, "Email Testing");
    }
}
