package com.alurkerja.bpmn;

import com.alurkerja.DemoApplication;
import com.alurkerja.leave.LeaveService;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.camunda.bpm.model.xml.type.ModelElementType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

@SpringBootTest(classes = DemoApplication.class)
public class ProcessDefinitionTest {

    @Autowired
    LeaveService leaveService;

    @Test
    public void xml(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(leaveService.getProcessDefinitionKey())
                .latestVersion().singleResult();
        Assertions.assertNotNull(processDefinition);

        Assertions.assertNotNull(repositoryService.getProcessModel(processDefinition.getId()));
    }

    @Test
    public void getXml(){
        Assertions.assertNotNull(this.leaveService.getBpmnXml());
    }

    @Test
    public void getTaskDefinitions() throws IOException {
        InputStream inputStream = leaveService.getBpmnXml();
        BpmnModelInstance bpmnModelInstance = Bpmn.readModelFromStream(inputStream);
        ModelElementType taskType = bpmnModelInstance.getModel().getType(UserTask.class);
        Collection<ModelElementInstance> taskInstances = bpmnModelInstance.getModelElementsByType(taskType);
        System.out.println("JUMLAH : " + taskInstances.size());
        for(ModelElementInstance x : taskInstances){
            System.out.println(x.getAttributeValue("id"));
        }

        Assertions.assertNotNull(this.leaveService.getBpmnXml());
        Assertions.assertEquals(true, taskInstances.size()>0);
    }

    @Test
    public void statistic(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<String> taskDefinitionKeys = leaveService.getTaskDefinitionKeys();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        List<ProcessInstance> processInstances = runtimeService.createProcessInstanceQuery().processDefinitionKey(leaveService.getProcessDefinitionKey()).active().list();
        System.out.println("PROCESS INSTANCES : " + processInstances.size());
        for(ProcessInstance processInstance : processInstances){
            System.out.println(processInstance.getId());
        }
        List<Task> tasks = taskService.createTaskQuery().active().list();
        System.out.println("LIST TASKS : " + tasks.size());
        System.out.println("ALL TASK : " + taskService.createTaskQuery().count());
        System.out.println("JUMLAH TASK : " + taskService.createTaskQuery().processDefinitionKey(leaveService.getProcessDefinitionKey()).count());
        for(String taskDef : taskDefinitionKeys){
            System.out.println(taskDef);
        }
        System.out.println(leaveService.getStatistic(null));
    }
}
