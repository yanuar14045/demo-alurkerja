package com.alurkerja.crud.jatahcuti;

import com.alurkerja.DemoApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest(classes = DemoApplication.class)
class JatahCutiServiceTest {

    @Autowired
    JatahCutiRepository jatahCutiRepository;

    @Autowired
    JatahCutiService jatahCutiService;

    @Test
    public void helloTest(){
        System.out.println("Hello world");
        Optional<JatahCuti> jatahCutiOptional = jatahCutiRepository.findById(UUID.fromString("xxxx"));
        Assertions.assertFalse(jatahCutiOptional.isPresent());
    }

    @Test
    public void getField(){
        JatahCuti jatahCuti = new JatahCuti();

        Class clazz = jatahCuti.getClass();

        for(Field field : clazz.getDeclaredFields()){
            System.out.println(field.getName());
        }
    }
}