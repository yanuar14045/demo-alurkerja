package com.alurkerja.crud.jabatan;

import com.alurkerja.DemoApplication;
import com.alurkerja.core.exception.CrudException;
import com.alurkerja.core.util.FieldValidationUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@SpringBootTest(classes = DemoApplication.class)
public class JabatanServiceTest {
    Logger logger = Logger.getLogger(getClass().getName());
    @Autowired
    JabatanService jabatanService;

    @Test
    void cleansingData(){
        logger.info("Cleansing data jabatan");
        List<Jabatan> jabatanList = jabatanService.findAll();
        if(jabatanList.size()>0){
            jabatanList.forEach(jabatan -> {
                try {
                    jabatanService.delete(jabatan);
                } catch (CrudException e) {
                    e.printStackTrace();
                }
            });
        }
        jabatanList = jabatanService.findAll();
        Assertions.assertEquals(jabatanList.size(), 0);
    }

    @Test
    void create() throws CrudException {
        String name = "XXXXXXXXXXXXXXXXXX";

        logger.info("Check if instantiateDto is not null");
        JabatanDto dto = jabatanService.instantiateDto();
        Assertions.assertNotNull(dto);

        dto.setName(name);
        Page<Jabatan> jabatanOptional = jabatanService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(true, jabatanOptional.isEmpty());


        Jabatan jabatan = jabatanService.createFromDto(dto);
        logger.info("Check if jabatan is not null");
        Assertions.assertNotNull(jabatan);
        logger.info("Check if jabatan.id is not null");
        Assertions.assertNotNull(jabatan.getId());
        logger.info("Check if jabatan.name is equal with : " + name);
        Assertions.assertEquals(name, jabatan.getName());
    }

    @Test
    void update(){
        String name = "XXXXXXXXXXXXXXXXXX";

        logger.info("Check if instantiateDto is not null");
        JabatanDto dto = jabatanService.instantiateDto();
        Assertions.assertNotNull(dto);

        dto.setName(name);
        Page<Jabatan> jabatanOptional = jabatanService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(false, jabatanOptional.isEmpty());

        Jabatan jabatan = jabatanOptional.toList().get(0);
        String updatedName = "YYYYYYYYYYY";
        jabatan.setName(updatedName);

        Assertions.assertNull(jabatan.getUpdatedDate());
        jabatan = jabatanService.update(jabatan);
        Assertions.assertNotNull(jabatan.getUpdatedDate());
        Assertions.assertEquals(updatedName, jabatan.getName());

        jabatanOptional = jabatanService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(true, jabatanOptional.isEmpty());
    }

    @Test
    void updateByDto() throws CrudException {
        Jabatan jabatan = jabatanService.findAll().get(0);
        Assertions.assertNotNull(jabatan);
        JabatanDto jabatanDto = new JabatanDto();
        String zzz = "ZZZZZZZZZZZ";
        jabatanDto.setName(zzz);
        jabatanService.update(jabatan, jabatanDto);
        Assertions.assertEquals(zzz, jabatan.getName());
    }

    @Test
    void toDto(){
        Jabatan jabatan = jabatanService.findAll().get(0);
        JabatanDto dto = new JabatanDto();
        Assertions.assertNotEquals(dto.getName(), jabatan.getName());
        dto = dto.toDto(jabatan);
        Assertions.assertEquals(dto.getName(), jabatan.getName());
    }

    @Test
    void fromDto(){
        JabatanDto dto = new JabatanDto();
        dto.setName("ABCD");
        Jabatan jabatan = new Jabatan();
        Assertions.assertNotEquals(jabatan.getName(), dto.getName());
        dto.copyFromDto(jabatan);
        Assertions.assertEquals(jabatan.getName(), dto.getName());
    }

    @Test
    void dtoValidationTest(){
        JabatanDto jabatanDto = new JabatanDto();
        jabatanDto.setName("name");
        FieldValidationUtil<JabatanDto> fieldValidationUtil = new FieldValidationUtil<>();
        Assertions.assertNotNull(fieldValidationUtil);
    }

    @Test
    void exportExcel() throws IOException {
        List<Jabatan> categories = jabatanService.findAll();
        Jabatan jabatan = new Jabatan();
        jabatan.setName("Kategori 1");
        categories.add(jabatan);

        Jabatan jabatan1 = new Jabatan();
        jabatan1.setName("Kategori 3");
        categories.add(jabatan1);
        JabatanToExcel jabatanToExcel = new JabatanToExcel(categories);
        FileOutputStream fileOutputStream = new FileOutputStream("jabatan.xlsx");
        jabatanToExcel.export(fileOutputStream);
        fileOutputStream.close();
    }
}
