package com.alurkerja.crud.article;

import com.alurkerja.crud.category.CategoryDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class ArticleSpecificationTest {
    Logger logger = Logger.getLogger(getClass().getName());
    @Test
    void getPropertyValue(){
        ArticleDto articleDto = new ArticleDto();
        String articleTitle = "title";
        articleDto.setTitle(articleTitle);
        CategoryDto categoryDto = new CategoryDto();
        String categoryName = "Category xxxx";
        categoryDto.setName(categoryName);
        articleDto.setCategory(categoryDto);

        ArticleSpecification articleSpecification = new ArticleSpecification(articleDto);

        Assertions.assertEquals(articleTitle, articleSpecification.getPropertyValue("title", articleDto));
        Assertions.assertEquals(categoryName, articleSpecification.getPropertyValue("category.name", articleDto));
        Assertions.assertNull(articleSpecification.getPropertyValue("category.nama", articleDto));
        Assertions.assertNotNull(articleDto);
    }

}