package com.alurkerja.crud.school;

import com.alurkerja.DemoApplication;
import com.alurkerja.core.exception.CrudException;
import com.alurkerja.core.util.FieldValidationUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.logging.Logger;

@SpringBootTest(classes = DemoApplication.class)
public class SchoolServiceTest {
    Logger logger = Logger.getLogger(getClass().getName());
    @Autowired
    SchoolService schoolService;

    @Test
    void cleansingData(){
        logger.info("Cleansing data school");
        List<School> schoolList = schoolService.findAll();
        if(schoolList.size()>0){
            schoolList.forEach(school -> {
                try {
                    schoolService.delete(school);
                } catch (CrudException e) {
                    e.printStackTrace();
                }
            });
        }
        schoolList = schoolService.findAll();
        Assertions.assertEquals(schoolList.size(), 0);
    }

    @Test
    void create() throws CrudException {
        String name = "XXXXXXXXXXXXXXXXXX";

        logger.info("Check if instantiateDto is not null");
        SchoolDto dto = schoolService.instantiateDto();
        Assertions.assertNotNull(dto);

        dto.setName(name);
        Page<School> schoolOptional = schoolService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(true, schoolOptional.isEmpty());


        School school = schoolService.createFromDto(dto);
        logger.info("Check if school is not null");
        Assertions.assertNotNull(school);
        logger.info("Check if school.id is not null");
        Assertions.assertNotNull(school.getId());
        logger.info("Check if school.name is equal with : " + name);
        Assertions.assertEquals(name, school.getName());
    }

    @Test
    void update(){
        String name = "XXXXXXXXXXXXXXXXXX";

        logger.info("Check if instantiateDto is not null");
        SchoolDto dto = schoolService.instantiateDto();
        Assertions.assertNotNull(dto);

        dto.setName(name);
        Page<School> schoolOptional = schoolService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(false, schoolOptional.isEmpty());

        School school = schoolOptional.toList().get(0);
        String updatedName = "YYYYYYYYYYY";
        school.setName(updatedName);

        Assertions.assertNull(school.getUpdatedDate());
        school = schoolService.update(school);
        Assertions.assertNotNull(school.getUpdatedDate());
        Assertions.assertEquals(updatedName, school.getName());

        schoolOptional = schoolService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(true, schoolOptional.isEmpty());
    }

    @Test
    void updateByDto() throws CrudException {
        School school = schoolService.findAll().get(0);
        Assertions.assertNotNull(school);
        SchoolDto schoolDto = new SchoolDto();
        String zzz = "ZZZZZZZZZZZ";
        schoolDto.setName(zzz);
        schoolService.update(school, schoolDto);
        Assertions.assertEquals(zzz, school.getName());
    }

    @Test
    void toDto(){
        School school = schoolService.findAll().get(0);
        SchoolDto dto = new SchoolDto();
        Assertions.assertNotEquals(dto.getName(), school.getName());
        dto = dto.toDto(school);
        Assertions.assertEquals(dto.getName(), school.getName());
    }

    @Test
    void fromDto(){
        SchoolDto dto = new SchoolDto();
        dto.setName("ABCD");
        School school = new School();
        Assertions.assertNotEquals(school.getName(), dto.getName());
        dto.copyFromDto(school);
        Assertions.assertEquals(school.getName(), dto.getName());
    }

    @Test
    void dtoValidationTest(){
        SchoolDto schoolDto = new SchoolDto();
        schoolDto.setName("name");
        FieldValidationUtil<SchoolDto> fieldValidationUtil = new FieldValidationUtil<>();
        Assertions.assertNotNull(fieldValidationUtil);
    }
}
