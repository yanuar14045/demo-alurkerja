package com.alurkerja.crud.category;

import com.alurkerja.DemoApplication;
import com.alurkerja.core.exception.CrudException;
import com.alurkerja.core.util.FieldValidationUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@SpringBootTest(classes = DemoApplication.class)
public class CategoryServiceTest {
    Logger logger = Logger.getLogger(getClass().getName());
    @Autowired
    CategoryService categoryService;

    @Test
    void cleansingData(){
        logger.info("Cleansing data category");
        List<Category> categoryList = categoryService.findAll();
        if(categoryList.size()>0){
            categoryList.forEach(category -> {
                try {
                    categoryService.delete(category);
                } catch (CrudException e) {
                    e.printStackTrace();
                }
            });
        }
        categoryList = categoryService.findAll();
        Assertions.assertEquals(categoryList.size(), 0);
    }

    @Test
    void create() throws CrudException {
        String name = "XXXXXXXXXXXXXXXXXX";

        logger.info("Check if instantiateDto is not null");
        CategoryDto dto = categoryService.instantiateDto();
        Assertions.assertNotNull(dto);

        dto.setName(name);
        Page<Category> categoryOptional = categoryService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(true, categoryOptional.isEmpty());


        Category category = categoryService.createFromDto(dto);
        logger.info("Check if category is not null");
        Assertions.assertNotNull(category);
        logger.info("Check if category.id is not null");
        Assertions.assertNotNull(category.getId());
        logger.info("Check if category.name is equal with : " + name);
        Assertions.assertEquals(name, category.getName());
    }

    @Test
    void update(){
        String name = "XXXXXXXXXXXXXXXXXX";

        logger.info("Check if instantiateDto is not null");
        CategoryDto dto = categoryService.instantiateDto();
        Assertions.assertNotNull(dto);

        dto.setName(name);
        Page<Category> categoryOptional = categoryService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(false, categoryOptional.isEmpty());

        Category category = categoryOptional.toList().get(0);
        String updatedName = "YYYYYYYYYYY";
        category.setName(updatedName);

        Assertions.assertNull(category.getUpdatedDate());
        category = categoryService.update(category);
        Assertions.assertNotNull(category.getUpdatedDate());
        Assertions.assertEquals(updatedName, category.getName());

        categoryOptional = categoryService.findAll(dto, PageRequest.of(0, 100));
        logger.info("Check if database is empty");
        Assertions.assertEquals(true, categoryOptional.isEmpty());
    }

    @Test
    void updateByDto() throws CrudException {
        Category category = categoryService.findAll().get(0);
        Assertions.assertNotNull(category);
        CategoryDto categoryDto = new CategoryDto();
        String zzz = "ZZZZZZZZZZZ";
        categoryDto.setName(zzz);
        categoryService.update(category, categoryDto);
        Assertions.assertEquals(zzz, category.getName());
    }

    @Test
    void toDto(){
        Category category = categoryService.findAll().get(0);
        CategoryDto dto = new CategoryDto();
        Assertions.assertNotEquals(dto.getName(), category.getName());
        dto = dto.toDto(category);
        Assertions.assertEquals(dto.getName(), category.getName());
    }

    @Test
    void fromDto(){
        CategoryDto dto = new CategoryDto();
        dto.setName("ABCD");
        Category category = new Category();
        Assertions.assertNotEquals(category.getName(), dto.getName());
        dto.copyFromDto(category);
        Assertions.assertEquals(category.getName(), dto.getName());
    }

    @Test
    void dtoValidationTest(){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName("name");
        FieldValidationUtil<CategoryDto> fieldValidationUtil = new FieldValidationUtil<>();
        Assertions.assertNotNull(fieldValidationUtil);
    }

    @Test
    void exportExcel() throws IOException {
        List<Category> categories = categoryService.findAll();
        Category category = new Category();
        category.setName("Kategori 1");
        categories.add(category);

        Category category1 = new Category();
        category1.setName("Kategori 3");
        categories.add(category1);
        CategoryToExcel categoryToExcel = new CategoryToExcel(categories);
        FileOutputStream fileOutputStream = new FileOutputStream("category.xlsx");
        categoryToExcel.export(fileOutputStream);
        fileOutputStream.close();
    }
}
